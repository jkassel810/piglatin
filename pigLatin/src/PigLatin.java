/**
 * Created by jkass on 11/26/2016.
 */

public class PigLatin {

    public static String pigLatin(String myString){
        String[] mySentence = myString.split(" ");
        String newString = "";
        String endChar = "";
        for (int i=0; i< mySentence.length; i++){
            endChar = " ";
            if (i == mySentence.length-1){
                if (mySentence[i].endsWith(".")){
                    mySentence[i] = mySentence[i].substring(0,mySentence[i].length()-1);
                    endChar = ".";
                }
            }
            newString += mySentence[i].substring(1,mySentence[i].length()) + "-" + mySentence[i].substring(0,1) + "ay" + endChar;
        }
        return newString;

    }

    public static void main(String[] args){

        String testSentence = "THis is my test sentence.";
        System.out.println("Test Sentence: " + testSentence);

        String myResult = pigLatin(testSentence);

        System.out.println(myResult);


    }


}
